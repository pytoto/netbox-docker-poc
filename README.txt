This is a POC to demonstrate the customisation of a dockerized Netbox


Build image
-----------

>>> docker build . -t local/netbox


Run container
-------------
You must have a running Redis instance (directly on host)
You must have a running Postgres (in a docker container)

>>> docker run --rm -d -e POSTGRES_HOST=<ip@ of postgres container> --network=host --name netbox local/netbox
>>> docker logs netbox -f

You can get the ip@ of the running postgres container :
>>> docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' postgres
