
ARG PYTHON_VERSION=3.10.13
FROM python:${PYTHON_VERSION}-slim

LABEL description="POC Netbox with custom configuration.py and french django.po"

# DL3008 - Require pinned dependencies for apt install
# hadolint ignore=DL3008
RUN --mount=type=cache,target="/var/cache/apt",sharing=locked \
    --mount=type=cache,target="/var/lib/apt/lists",sharing=locked \
    apt-get update && \
    apt-get upgrade -y && \
    apt-get install --no-install-recommends -y unzip gettext mime-support libxml2 openssl && \
    apt-get autoremove -y

# copy netbox source to /
COPY netbox-develop.zip /
WORKDIR /
RUN unzip netbox-develop.zip && \
    rm netbox-develop.zip

# DL3013 - pin all Python package versions
# hadolint ignore=DL3013
RUN pip install --no-cache-dir -U pip wheel \
    pip install --no-cache-dir -r /netbox-develop/requirements.txt

# overwrite configuration.py and django.po for french
COPY configuration.py /netbox-develop/netbox/configuration.py
COPY django.po /netbox-develop/netbox/translations/fr/LC_MESSAGES/django.po
COPY entry-point.sh /entry-point.sh
RUN chmod a+x /entry-point.sh

EXPOSE 8000
ENTRYPOINT /entry-point.sh
