#!/bin/bash

# Stop when an error occurs
set -e

cd /netbox-develop/netbox
export NETBOX_CONFIGURATION=configuration

./manage.py collectstatic --noinput --link
./manage.py compilemessages -l fr

./manage.py migrate

# set superuser admin password
./manage.py shell --interface python << END
from django.contrib.auth import get_user_model
u = get_user_model().objects.get(username='admin')
u.set_password('admin')
u.save()
END

./manage.py runserver 0.0.0.0:8000 --insecure
